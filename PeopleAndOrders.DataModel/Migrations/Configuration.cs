using PeopleAndOrders.DataModel.Classes;
namespace PeopleAndOrders.DataModel.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PeopleAndOrders.DataModel.Classes.PaOContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PeopleAndOrders.DataModel.Classes.PaOContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Addresses.AddOrUpdate(
                a => a.ID,
                new Address { ID = 0, City = "Rzesz�w", Street = "Graniczna", BuildingNumber = 1, FlatNumber = 32, PostalCode = "35-326" },
                new Address { ID = 1, City = "Rzesz�w", Street = "Graniczna", BuildingNumber = 1, FlatNumber = 37, PostalCode = "35-326" },
                new Address { ID = 2, City = "Rzesz�w", Street = "Kwiatkowskiego", BuildingNumber = 8, FlatNumber = 56, PostalCode = "35-326" },
                new Address { ID = 3, City = "Rzesz�w", Street = "Kwiatkowskiego", BuildingNumber = 17, FlatNumber = 2, PostalCode = "35-326" },
                new Address { ID = 4, City = "Rzesz�w", Street = "Marsza�kowska", BuildingNumber = 21, FlatNumber = 56, PostalCode = "35-300" },
                new Address { ID = 5, City = "Rzesz�w", Street = "Grochowa", BuildingNumber = 10, FlatNumber = 4, PostalCode = "35-318" },
                new Address { ID = 6, City = "Krak�w", Street = "Mo�dawska", BuildingNumber = 56, FlatNumber = 19, PostalCode = "30-412" },
                new Address { ID = 7, City = "Krak�w", Street = "Geodet�w", BuildingNumber = 12, FlatNumber = 167, PostalCode = "30-412" },
                new Address { ID = 8, City = "Krak�w", Street = "Chrobrego", BuildingNumber = 2, FlatNumber = 2, PostalCode = "30-412" },
                new Address { ID = 9, City = "Pozna�", Street = "Jagielo�ska", BuildingNumber = 21, FlatNumber = 9, PostalCode = "61-622" },
                new Address { ID = 10, City = "Warszawa", Street = "G��boka", BuildingNumber = 52, FlatNumber = 14, PostalCode = "01-072" }
                );

            context.Customers.AddOrUpdate(
                c => c.ID,
                new Customer { ID = 0, FirstName = "Grzegorz", LastName = "Kowalski", AddressID = 1, Email = "gkowalski@mail.com", Phone = 876313267 },
                new Customer { ID = 1, FirstName = "Miros�aw", LastName = "Kowalski", AddressID = 8, Email = "mkowalski@mail.com", Phone = 467943267 },
                new Customer { ID = 2, FirstName = "Dariusz", LastName = "Kowalski", AddressID = 9, Email = "kowalskidariusz@mail.com", Phone = 469345267 },
                new Customer { ID = 3, FirstName = "Jan", LastName = "Kowalski", AddressID = 2, Email = "jankowalski@mail.com", Phone = 123456789 },
                new Customer { ID = 4, FirstName = "Maria", LastName = "Kowalska", AddressID = 2, Email = "mariakowalska@mail.com", Phone = 467943981 },
                new Customer { ID = 5, FirstName = "Teresa", LastName = "Mila", AddressID = 3, Email = "tereska@mail.com", Phone = 123654890 },
                new Customer { ID = 6, FirstName = "Andrzej", LastName = "Nowak", AddressID = 4, Email = "anowak@mail.com", Phone = 946186734 },
                new Customer { ID = 7, FirstName = "Wanda", LastName = "Nowak", AddressID = 4, Email = "wn@mail.com", Phone = 576234987 },
                new Customer { ID = 8, FirstName = "Kacper", LastName = "Pili�ski", AddressID = 5, Email = "pilinski@mail.com", Phone = 657432897 },
                new Customer { ID = 9, FirstName = "Jan", LastName = "Gawron", AddressID = 6, Email = "jgawron@mail.com", Phone = 875345090 },
                new Customer { ID = 10, FirstName = "Tomasz", LastName = "Gawron", AddressID = 7, Email = "tomaszg@mail.com", Phone = 456723999 },
                new Customer { ID = 11, FirstName = "Tadeusz", LastName = "Rejtan", AddressID = 10, Email = "rejtan@mail.com", Phone = 234567899 },
                new Customer { ID = 12, FirstName = "Krystyna", LastName = "Rejtan", AddressID = 10, Email = "rejtan@mail.com", Phone = 234567899 }
                );

            context.ServiceProviders.AddOrUpdate(
                p => p.ID,
                new ServiceProvider { ID = 0, Name = "Orange" },
                new ServiceProvider { ID = 1, Name = "Play" },
                new ServiceProvider { ID = 2, Name = "Plus" },
                new ServiceProvider { ID = 3, Name = "T-Mobile" }
                );

            context.Services.AddOrUpdate(
                s => s.ID,
                new Service { ID = 0, Name = "Smart Plan LTE 1", ServiceProviderID = 0, Duration = 24, MonthlyPrice = 34.99m, Description = "1GB LTE, nielimitowane rozmowy i smsy do wszystkich." },
                new Service { ID = 1, Name = "Smart Plan LTE 2", ServiceProviderID = 0, Duration = 24, MonthlyPrice = 54.99m, Description = "2GB LTE, nielimitowane rozmowy i smsy do wszystkich, roaming 50min 50mb." },
                new Service { ID = 2, Name = "Smart Plan LTE 3", ServiceProviderID = 0, Duration = 24, MonthlyPrice = 74.99m, Description = "5GB LTE, nielimitowane rozmowy i smsy do wszystkich, roaming 400min, 100mb." },
                new Service { ID = 4, Name = "Formu�a Unlimited 1", ServiceProviderID = 1, Duration = 24, MonthlyPrice = 9.99m, Description = "500MB LTE, nielimitowane rozmowy i smsy w sieci." },
                new Service { ID = 5, Name = "Formu�a Unlimited 1", ServiceProviderID = 1, Duration = 12, MonthlyPrice = 15.98m, Description = "500MB LTE, nielimitowane rozmowy i smsy w sieci." },
                new Service { ID = 6, Name = "Formu�a Unlimited 2", ServiceProviderID = 1, Duration = 24, MonthlyPrice = 39.99m, Description = "4GB LTE, nielimitowane rozmowy i smsy do wszystkich, nielimitowany internet przez 6 miesi�cy." },
                new Service { ID = 7, Name = "Formu�a Unlimited 2", ServiceProviderID = 1, Duration = 12, MonthlyPrice = 45.98m, Description = "4GB LTE, nielimitowane rozmowy i smsy do wszystkich, nielimitowany internet przez 6 miesi�cy." },
                new Service { ID = 8, Name = "Abonament miesi�czny 79.99", ServiceProviderID = 2, Duration = 24, MonthlyPrice = 79.99m, Description = "5GB LTE, nielimitowane rozmowy i smsy do wszystkich, nielimitowany internet przez 3 miesi�cy, roaming 120min." },
                new Service { ID = 9, Name = "Multi", ServiceProviderID = 3, Duration = 24, MonthlyPrice = 99.99m, Description = "10GB LTE, nielimitowane rozmowy i smsy do wszystkich, roaming 30min." }
                );

            context.Sections.AddOrUpdate(
                s => s.ID,
                new Section { ID = 0, Name = "Sekcja 1", ManagerID = 0},
                new Section { ID = 1, Name = "Sekcja 2", ManagerID = 1 }
                );

            context.Employees.AddOrUpdate(
                e => e.ID,
                new Employee { ID = 0, AddressID = 1, FirstName = "Jadwiga", LastName = "Chodacka", Phone = 501334556, Email = "jchodacka@korpo.com", Position = Enums.EmployeePosition.Director},
                new Employee { ID = 1, AddressID = 2, FirstName = "Karol", LastName = "Piasecki", Phone = 321321321, Email = "piaseckik@korpo.com", Position = Enums.EmployeePosition.Manager, SectionID = 0},
                new Employee { ID = 2, AddressID = 4, FirstName = "Tomasz", LastName = "Jakubiak", Phone = 454545455, Email = "jakubiak@korpo.com", Position = Enums.EmployeePosition.Manager, SectionID = 1 },
                new Employee { ID = 3, AddressID = 5, FirstName = "Katarzyna", LastName = "Lis", Phone = 985672344, Email = "klis@korpo.com", Position = Enums.EmployeePosition.Consultant, SectionID = 0 },
                new Employee { ID = 4, AddressID = 6, FirstName = "Waldemar", LastName = "Sowa", Phone = 115672344, Email = "wsowa@korpo.com", Position = Enums.EmployeePosition.Consultant, SectionID = 0 },
                new Employee { ID = 5, AddressID = 7, FirstName = "Damian", LastName = "Andrzejewski", Phone = 888672344, Email = "dandrzejewski@korpo.com", Position = Enums.EmployeePosition.Consultant, SectionID = 1 },
                new Employee { ID = 6, AddressID = 9, FirstName = "Robert", LastName = "Sikora", Phone = 575900023, Email = "rsikora@korpo.com", Position = Enums.EmployeePosition.Consultant, SectionID = 1 },
                new Employee { ID = 7, AddressID = 6, FirstName = "Mariola", LastName = "Sowa", Phone = 604345543, Email = "msowa@korpo.com", Position = Enums.EmployeePosition.Consultant, SectionID = 1 }
                );

            context.Contracts.AddOrUpdate(
                c => c.ID,
                new Contract { ID = 0, CustomerID = 0, SellerID = 3, ServiceID = 0, DateBeginning = new DateTime(2015, 8, 12)},
                new Contract { ID = 1, CustomerID = 1, SellerID = 4, ServiceID = 1, DateBeginning = new DateTime(2015, 4, 23) },
                new Contract { ID = 2, CustomerID = 2, SellerID = 5, ServiceID = 2, DateBeginning = new DateTime(2014, 9, 14) },
                new Contract { ID = 3, CustomerID = 3, SellerID = 6, ServiceID = 3, DateBeginning = new DateTime(2013, 1, 19) },
                new Contract { ID = 4, CustomerID = 4, SellerID = 7, ServiceID = 4, DateBeginning = new DateTime(2014, 2, 22) },
                new Contract { ID = 5, CustomerID = 5, SellerID = 3, ServiceID = 5, DateBeginning = new DateTime(2013, 3, 26) },
                new Contract { ID = 6, CustomerID = 6, SellerID = 4, ServiceID = 6, DateBeginning = new DateTime(2014, 4, 28) },
                new Contract { ID = 7, CustomerID = 7, SellerID = 5, ServiceID = 7, DateBeginning = new DateTime(2016, 5, 11) },
                new Contract { ID = 8, CustomerID = 8, SellerID = 6, ServiceID = 8, DateBeginning = new DateTime(2014, 8, 13) },
                new Contract { ID = 9, CustomerID = 9, SellerID = 7, ServiceID = 9, DateBeginning = new DateTime(2015, 4, 14) },
                new Contract { ID = 10, CustomerID = 10, SellerID = 3, ServiceID = 1, DateBeginning = new DateTime(2016, 2, 8) },
                new Contract { ID = 11, CustomerID = 12, SellerID = 4, ServiceID = 2, DateBeginning = new DateTime(2015, 6, 7) },
                new Contract { ID = 12, CustomerID = 1, SellerID = 5, ServiceID = 3, DateBeginning = new DateTime(2015, 7, 3) },
                new Contract { ID = 13, CustomerID = 2, SellerID = 6, ServiceID = 4, DateBeginning = new DateTime(2015, 7, 1) },
                new Contract { ID = 14, CustomerID = 2, SellerID = 7, ServiceID = 5, DateBeginning = new DateTime(2016, 1, 2) },
                new Contract { ID = 15, CustomerID = 3, SellerID = 4, ServiceID = 6, DateBeginning = new DateTime(2015, 2, 5) },
                new Contract { ID = 16, CustomerID = 4, SellerID = 4, ServiceID = 7, DateBeginning = new DateTime(2016, 3, 12) },
                new Contract { ID = 17, CustomerID = 5, SellerID = 5, ServiceID = 8, DateBeginning = new DateTime(2015, 11, 13) },
                new Contract { ID = 18, CustomerID = 5, SellerID = 5, ServiceID = 9, DateBeginning = new DateTime(2014, 11, 13) },
                new Contract { ID = 19, CustomerID = 6, SellerID = 5, ServiceID = 1, DateBeginning = new DateTime(2015, 12, 23) },
                new Contract { ID = 20, CustomerID = 7, SellerID = 6, ServiceID = 2, DateBeginning = new DateTime(2015, 4, 12) },
                new Contract { ID = 21, CustomerID = 7, SellerID = 6, ServiceID = 3, DateBeginning = new DateTime(2015, 4, 12) },
                new Contract { ID = 22, CustomerID = 7, SellerID = 6, ServiceID = 4, DateBeginning = new DateTime(2015, 4, 12) },
                );

            context.Orders.AddOrUpdate(
                o => o.ID,
                new Order { ID = 0, CustomerID = 1, SellerID = 3, ServiceID = 1, Date = new DateTime(2016, 5, 20), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje."},
                new Order { ID = 1, CustomerID = 2, SellerID = 4, ServiceID = 2, Date = new DateTime(2016, 4, 12), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 2, CustomerID = 3, SellerID = 5, ServiceID = 3, Date = new DateTime(2016, 5, 15), OrderStatus = Enums.OrderStatus.Processing, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 3, CustomerID = 4, SellerID = 6, ServiceID = 4, Date = new DateTime(2016, 4, 15), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 4, CustomerID = 5, SellerID = 7, ServiceID = 5, Date = new DateTime(2016, 4, 17), OrderStatus = Enums.OrderStatus.Added, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 5, CustomerID = 6, SellerID = 3, ServiceID = 6, Date = new DateTime(2016, 5, 18), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 6, CustomerID = 7, SellerID = 4, ServiceID = 7, Date = new DateTime(2016, 4, 1), OrderStatus = Enums.OrderStatus.Processing, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 7, CustomerID = 8, SellerID = 5, ServiceID = 8, Date = new DateTime(2016, 4, 3), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 8, CustomerID = 9, SellerID = 6, ServiceID = 9, Date = new DateTime(2016, 5, 4), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 9, CustomerID = 10, SellerID = 7, ServiceID = 1, Date = new DateTime(2016, 3, 29), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 10, CustomerID = 11, SellerID = 3, ServiceID = 2, Date = new DateTime(2016, 3, 7), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 11, CustomerID = 12, SellerID = 4, ServiceID = 3, Date = new DateTime(2016, 5, 7), OrderStatus = Enums.OrderStatus.Processing, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 12, CustomerID = 1, SellerID = 5, ServiceID = 4, Date = new DateTime(2016, 4, 9), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 13, CustomerID = 2, SellerID = 6, ServiceID = 5, Date = new DateTime(2016, 5, 20), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 14, CustomerID = 3, SellerID = 7, ServiceID = 6, Date = new DateTime(2016, 5, 20), OrderStatus = Enums.OrderStatus.Added, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 15, CustomerID = 4, SellerID = 3, ServiceID = 7, Date = new DateTime(2016, 4, 21), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 16, CustomerID = 5, SellerID = 4, ServiceID = 8, Date = new DateTime(2016, 5, 22), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 17, CustomerID = 6, SellerID = 5, ServiceID = 9, Date = new DateTime(2016, 3, 16), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 18, CustomerID = 7, SellerID = 6, ServiceID = 1, Date = new DateTime(2016, 5, 12), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 19, CustomerID = 8, SellerID = 7, ServiceID = 2, Date = new DateTime(2016, 5, 9), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 20, CustomerID = 9, SellerID = 4, ServiceID = 3, Date = new DateTime(2016, 4, 13), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 21, CustomerID = 10, SellerID = 5, ServiceID = 4, Date = new DateTime(2016, 5, 6), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 22, CustomerID = 11, SellerID = 6, ServiceID = 5, Date = new DateTime(2016, 4, 3), OrderStatus = Enums.OrderStatus.Processing, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 23, CustomerID = 12, SellerID = 7, ServiceID = 6, Date = new DateTime(2016, 3, 13), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 24, CustomerID = 1, SellerID = 5, ServiceID = 7, Date = new DateTime(2016, 5, 15), OrderStatus = Enums.OrderStatus.Processing, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 25, CustomerID = 2, SellerID = 7, ServiceID = 8, Date = new DateTime(2016, 4, 20), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                new Order { ID = 26, CustomerID = 3, SellerID = 7, ServiceID = 9, Date = new DateTime(2016, 5, 20), OrderStatus = Enums.OrderStatus.Completed, AdditionalInfo = "Klient prosi o jak najszybsz� realizacje." },
                );
        }
    }
}
