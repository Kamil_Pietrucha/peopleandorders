﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PeopleAndOrders.DataModel.Enums;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Order
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [ForeignKey("Seller")]
        public int SellerID { get; set; }
        public virtual Employee Seller { get; set; }

        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }

        public int ServiceID { get; set; }
        public virtual Service Service { get; set; }

        [Display(Name ="Data"), Required(ErrorMessage ="Data złorzenia zamówienia wymagana.")]
        public DateTime Date { get; set; }

        [Display(Name ="Dodatkowe informacje")]
        public string AdditionalInfo { get; set; }

        public OrderStatus OrderStatus { get; set; }
    }
}
