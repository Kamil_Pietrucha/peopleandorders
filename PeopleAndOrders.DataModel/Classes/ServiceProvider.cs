﻿using System.ComponentModel.DataAnnotations;

namespace PeopleAndOrders.DataModel.Classes
{
    public class ServiceProvider
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Display(Name ="Nazwa"), Required(ErrorMessage ="Podaj nazwę usługodawcy.")]
        public string Name { get; set; }
    }
}
