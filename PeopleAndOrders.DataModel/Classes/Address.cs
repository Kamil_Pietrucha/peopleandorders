﻿using System.ComponentModel.DataAnnotations;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Address
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Display(Name = "Miasto"), Required(ErrorMessage = "Podaj miasto"), StringLength(40)]
        public string City { get; set; }

        [Display(Name = "Ulica"), Required(ErrorMessage = "Podaj ulicę."), StringLength(40)]
        public string Street { get; set; }

        [Display(Name = "Numer budynku"), Required(ErrorMessage = "Podaj numer budynku.")]
        public int BuildingNumber { get; set; }

        [Display(Name = "Numer mieszkania")]
        public int? FlatNumber { get; set; }

        [Display(Name = "Kod Pocztowy"), Required(ErrorMessage = "Podaj kod pocztowy")]
        public string PostalCode { get; set; }
    }
}
