﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Section
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Display(Name ="Nazwa"), Required(ErrorMessage ="Nazwa sekcji wymagana."), StringLength(50)]
        public string Name { get; set; }

        [ForeignKey("Manager")]
        public int ManagerID { get; set; }
        public virtual Employee Manager { get; set; }

        public virtual List<Employee> Employees { get; set; }
    }
}
