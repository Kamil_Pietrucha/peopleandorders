﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Contract
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        public int ServiceID { get; set; }
        public virtual Service Service { get; set; }

        [Display(Name ="Początek umowy")]
        public DateTime DateBeginning { get; set; }

        [Display(Name = "Koniec umowy")]
        public DateTime DateEnd
        {
            get
            {
                return DateBeginning.AddMonths(Service.Duration);
            }
        }

        [ForeignKey("Seller")]
        public int SellerID { get; set; }
        public virtual Employee Seller { get; set; }

        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
    }
}