﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PeopleAndOrders.DataModel.Enums;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Employee
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Display(Name = "Imie"), Required(ErrorMessage = "Podaj imie."), StringLength(40)]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko"), Required(ErrorMessage = "Podaj nazwisko."), StringLength(40)]
        public string LastName { get; set; }

        [Display(Name = "Stanowisko"), Required(ErrorMessage = "Wybierz stanowisko.")]
        public EmployeePosition Position { get; set; }

        [Display(Name = "Telefon")]
        public int Phone { get; set; }

        [Display(Name = "Email"), StringLength(40)]
        public string Email { get; set; }

        public int AddressID { get; set; }
        public virtual Address Address { get; set; }

        public int SectionID { get; set; }
        public virtual Section Section { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}