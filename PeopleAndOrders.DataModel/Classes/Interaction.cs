﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Interaction
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Display(Name ="Data"), Required]
        public DateTime Date { get; set; }

        [Display(Name ="Szczegóły")]
        public string Description { get; set; }

        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }

        public int CustomerID { get; set; }
        public Customer Customer { get; set; }
    }
}
