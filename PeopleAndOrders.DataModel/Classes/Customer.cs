﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Customer
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Display(Name ="Imię"), Required(ErrorMessage ="Podaj imię"), StringLength(40)]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko"), Required(ErrorMessage = "Podaj nazwisko"), StringLength(40)]
        public string LastName { get; set; }

        [Display(Name = "Telefon")]
        public int Phone { get; set; }

        [Display(Name = "Email"), StringLength(40)]
        public string Email { get; set; }

        public int AddressID { get; set; }
        public virtual Address Address { get; set; }

        public virtual List<Contract> Contracts { get; set; }
    }
}
