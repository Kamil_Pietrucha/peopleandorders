﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PeopleAndOrders.DataModel.Classes
{
    public class Service
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        public int ServiceProviderID { get; set; }
        public virtual ServiceProvider ServiceProvider { get; set; }

        [Display(Name = "Nazwa"), Required(ErrorMessage = "Podaj nazwę planu."), StringLength(150)]
        public string Name { get; set; }

        [Display(Name = "Opłata miesięczna")]
        public Decimal MonthlyPrice { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Długość umowy"), Required(ErrorMessage = "Podaj czas obowiązywania umowy.")]
        public int Duration { get; set; }
    }
}
