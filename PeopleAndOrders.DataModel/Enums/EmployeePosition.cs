﻿namespace PeopleAndOrders.DataModel.Enums
{
    public enum EmployeePosition
    {
        Consultant,
        Manager,
        Director
    }
}
