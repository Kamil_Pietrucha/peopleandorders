﻿namespace PeopleAndOrders.DataModel.Enums
{
    public enum OrderStatus
    {
        None,
        Added,
        Processing,
        Completed
    }
}
